package com.example.examencalculadora;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CalculadoraActivity extends AppCompatActivity {

    private Calculadora cal;
    private TextView lblUsuario, lblResultado;
    private EditText txtNumA, txtNumB;
    private Button btnSuma, btnResta, btnMulti, btnDiv, btnBack, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_calculadora);

        iniciarComponentes();

        this.btnLimpiar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                txtNumA.setText("");
                txtNumB.setText("");
                lblResultado.setText("");
            }
        });

        this.btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarVacios()==0){
                    Toast.makeText(CalculadoraActivity.this, "Ingrese los valores faltantes", Toast.LENGTH_SHORT).show();
                }
                else {
                    cal.setNumA(Float.parseFloat(txtNumA.getText().toString()));
                    cal.setNumB(Float.parseFloat(txtNumB.getText().toString()));

                    lblResultado.setText("Resultado: "+(String.valueOf(cal.suma())));

                }
            }
        });

        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarVacios()==0){
                    Toast.makeText(CalculadoraActivity.this, "Ingrese los valores faltantes", Toast.LENGTH_SHORT).show();
                }
                else {
                    cal.setNumA(Float.parseFloat(txtNumA.getText().toString()));
                    cal.setNumB(Float.parseFloat(txtNumB.getText().toString()));

                    lblResultado.setText("Resultado: "+(String.valueOf(cal.resta())));

                }
            }
        });

        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarVacios()==0){
                    Toast.makeText(CalculadoraActivity.this, "Ingrese los valores faltantes", Toast.LENGTH_SHORT).show();
                }
                else {
                    cal.setNumA(Float.parseFloat(txtNumA.getText().toString()));
                    cal.setNumB(Float.parseFloat(txtNumB.getText().toString()));

                    lblResultado.setText("Resultado: "+(String.valueOf(cal.multiplicar())));

                }
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarVacios()==0){
                    Toast.makeText(CalculadoraActivity.this, "Ingrese los valores faltantes", Toast.LENGTH_SHORT).show();
                }
                else {
                    cal.setNumA(Float.parseFloat(txtNumA.getText().toString()));
                    cal.setNumB(Float.parseFloat(txtNumB.getText().toString()));

                    lblResultado.setText("Resultado: "+(String.valueOf(cal.division())));

                }
            }
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });


    }
    public void iniciarComponentes(){
        lblUsuario =  findViewById(R.id.lblUsuario) ;
        txtNumA = (EditText) findViewById(R.id.txtNumA);
        txtNumB = (EditText) findViewById(R.id.txtNumB);
        lblResultado = (TextView) findViewById(R.id.lblResutaldo);
        btnSuma = (Button) findViewById(R.id.btnSuma);
        btnResta = (Button) findViewById(R.id.btnResta);
        btnMulti = (Button) findViewById(R.id.btnMulti);
        btnDiv = (Button) findViewById(R.id.btnDiv);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnBack = (Button) findViewById(R.id.btnBack);


        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        lblUsuario.setText("Usuario: " + nombre);
        this.cal= new Calculadora();

    }

    public int validarVacios(){
        if (txtNumA.getText().toString().matches("") ||
                txtNumB.getText().toString().matches("")  ){
            return 0;
        } else {
            return 1;
        }
    }

}